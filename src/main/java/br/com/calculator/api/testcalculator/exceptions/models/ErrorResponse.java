package br.com.calculator.api.testcalculator.exceptions.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse {
  private long timestamp;
  private String status;
  private int code;
  private String message;

  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private List<ErrorObject> erros;
}