package br.com.calculator.api.testcalculator.exceptions.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum QuantityEatExceptionEnum {

  QUANTITY_EAT_EXCEPTION_ENUM("CQE001", "Quantity Eat not found", 400);

  private String code;
  private String message;
  private Integer statusCode;

}