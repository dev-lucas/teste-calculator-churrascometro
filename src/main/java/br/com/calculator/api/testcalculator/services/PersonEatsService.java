package br.com.calculator.api.testcalculator.services;

import br.com.calculator.api.testcalculator.entities.PersonEatsEntity;

import java.util.List;

public interface PersonEatsService {

    List<PersonEatsEntity> findAll();
    PersonEatsEntity findById(Long id);
}