package br.com.calculator.api.testcalculator.facades.impl;

import br.com.calculator.api.testcalculator.dtos.responses.PercentageBarbecueResponseDTO;
import br.com.calculator.api.testcalculator.entities.PercentageBarbecueEntity;
import br.com.calculator.api.testcalculator.facades.PercentageBarbecueFacade;
import br.com.calculator.api.testcalculator.services.PercentageBarbecueService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PecentageBarbecueFacadeImpl implements PercentageBarbecueFacade {

  @Autowired
  private PercentageBarbecueService percentageBarbecueService;

  @Autowired
  private ModelMapper modelMapper;

  @Override
  public List<PercentageBarbecueResponseDTO> findAll() {
    List<PercentageBarbecueResponseDTO> percentageBarbecueResponseDTOS = new ArrayList<>();
    for (PercentageBarbecueEntity percentageBarbecueEntity : percentageBarbecueService.findAll()) {
      percentageBarbecueResponseDTOS.add(convertPercentageBarbecueEntityToPercentageBarbecueResponseDto(percentageBarbecueEntity));
    }
    return percentageBarbecueResponseDTOS;
  }

  @Override
  public PercentageBarbecueResponseDTO findById(Long id) {
    return convertPercentageBarbecueEntityToPercentageBarbecueResponseDto(percentageBarbecueService.findById(id));
  }

  private PercentageBarbecueResponseDTO convertPercentageBarbecueEntityToPercentageBarbecueResponseDto(PercentageBarbecueEntity percentageBarbecueEntity) {
    return modelMapper.map(percentageBarbecueEntity,PercentageBarbecueResponseDTO.class);
  }

}