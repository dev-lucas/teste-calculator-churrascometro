package br.com.calculator.api.testcalculator.facades;

import br.com.calculator.api.testcalculator.dtos.responses.QuantityEatResponseDTO;

import java.util.List;

public interface QuantityEatFacade {

  QuantityEatResponseDTO findById(Long id);
  List<QuantityEatResponseDTO> findAll();

}