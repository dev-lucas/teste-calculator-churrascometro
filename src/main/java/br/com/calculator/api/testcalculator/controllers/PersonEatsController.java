package br.com.calculator.api.testcalculator.controllers;

import br.com.calculator.api.testcalculator.dtos.responses.PersonEatsResponseDTO;
import br.com.calculator.api.testcalculator.facades.PersonEatsFacade;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/queries/person-eats")
public class PersonEatsController {

  @Autowired
  private PersonEatsFacade personEatsFacade;

  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Pessoas listadas com sucesso")
  })
  @GetMapping
  public List<PersonEatsResponseDTO> findAll() {
        return personEatsFacade.findAll();
    }

  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Pessoa listada com sucesso"),
      @ApiResponse(code = 400, message = "Pessoa noo encontrada")
  })
  @GetMapping("/{id}")
  public PersonEatsResponseDTO findById(Long id) {
    return personEatsFacade.findById(id);
  }

}