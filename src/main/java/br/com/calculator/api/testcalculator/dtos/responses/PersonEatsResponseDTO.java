package br.com.calculator.api.testcalculator.dtos.responses;

import br.com.calculator.api.testcalculator.enums.PersonEatsEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonEatsResponseDTO {

    private String id;
    private PersonEatsEnum personEatsEnum;
    private Double amount;

}