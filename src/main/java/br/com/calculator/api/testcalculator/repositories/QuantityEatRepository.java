package br.com.calculator.api.testcalculator.repositories;

import br.com.calculator.api.testcalculator.entities.QuantityEatEntity;
import org.springframework.data.repository.CrudRepository;

public interface QuantityEatRepository extends CrudRepository<QuantityEatEntity, Long> { }