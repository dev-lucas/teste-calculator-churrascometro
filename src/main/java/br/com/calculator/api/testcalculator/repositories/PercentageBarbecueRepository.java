package br.com.calculator.api.testcalculator.repositories;

import br.com.calculator.api.testcalculator.entities.PercentageBarbecueEntity;
import org.springframework.data.repository.CrudRepository;

public interface PercentageBarbecueRepository extends CrudRepository<PercentageBarbecueEntity, Long> { }