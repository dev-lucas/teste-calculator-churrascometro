package br.com.calculator.api.testcalculator.services;

import br.com.calculator.api.testcalculator.entities.PercentageBarbecueEntity;

import java.util.List;

public interface PercentageBarbecueService {

  List<PercentageBarbecueEntity> findAll();
  PercentageBarbecueEntity findById(Long id);

}