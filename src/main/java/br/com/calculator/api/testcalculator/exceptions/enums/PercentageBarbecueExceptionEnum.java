package br.com.calculator.api.testcalculator.exceptions.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum PercentageBarbecueExceptionEnum {

  PERCENTAGE_BARBECUE_EXCEPTION_ENUM("CPB001", "Percentage Barbecue not found", 400);

  private String code;
  private String message;
  private Integer statusCode;

}