package br.com.calculator.api.testcalculator.dtos.requests;

import br.com.calculator.api.testcalculator.enums.FoodEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PercentageBarbecueRequestDTO {

  private Long id;
  private FoodEnum foodEnum;
  private Double percentage;

}