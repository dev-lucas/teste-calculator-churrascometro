package br.com.calculator.api.testcalculator.controllers;

import br.com.calculator.api.testcalculator.dtos.responses.QuantityEatResponseDTO;
import br.com.calculator.api.testcalculator.facades.QuantityEatFacade;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/queries/quantity-eat")
public class QuantityEatController {

  @Autowired
  private QuantityEatFacade quantityEatFacade;

  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Niveis de fomes listados com sucesso")
  })
  @GetMapping
  public List<QuantityEatResponseDTO> findAll() {
    return quantityEatFacade.findAll();
  }

  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Nivel de fome listado com sucesso"),
      @ApiResponse(code = 400, message = "Nivel de fome nao encontrado")
  })
  @GetMapping("/{id}")
  public QuantityEatResponseDTO findById(Long id) {
    return quantityEatFacade.findById(id);
  }

}