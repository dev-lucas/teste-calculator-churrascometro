package br.com.calculator.api.testcalculator.facades.impl;

import br.com.calculator.api.testcalculator.dtos.responses.QuantityEatResponseDTO;
import br.com.calculator.api.testcalculator.entities.QuantityEatEntity;
import br.com.calculator.api.testcalculator.facades.QuantityEatFacade;
import br.com.calculator.api.testcalculator.services.QuantityEatService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class QuantityEatFacadeImpl implements QuantityEatFacade {

  @Autowired
  private QuantityEatService quantityEatService;

  @Autowired
  private ModelMapper modelMapper;

  @Override
  public List<QuantityEatResponseDTO> findAll() {
    List<QuantityEatResponseDTO> quantityEatResponseDTOS = new ArrayList<>();
    for (QuantityEatEntity quantityEatEntity : quantityEatService.findAll()) {
      quantityEatResponseDTOS.add(convertQuantityEatEntityToQuantityEatResponseDto(quantityEatEntity));
    }
    return quantityEatResponseDTOS;
  }

  @Override
  public QuantityEatResponseDTO findById(Long id) {
    return convertQuantityEatEntityToQuantityEatResponseDto(quantityEatService.findById(id));
  }

  private QuantityEatResponseDTO convertQuantityEatEntityToQuantityEatResponseDto(QuantityEatEntity quantityEatEntity) {
    return modelMapper.map(quantityEatEntity, QuantityEatResponseDTO.class);
  }

}