package br.com.calculator.api.testcalculator.facades;

import br.com.calculator.api.testcalculator.dtos.responses.PercentageBarbecueResponseDTO;

import java.util.List;

public interface PercentageBarbecueFacade {

  PercentageBarbecueResponseDTO findById(Long id);
  List<PercentageBarbecueResponseDTO> findAll();

}