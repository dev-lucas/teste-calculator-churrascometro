package br.com.calculator.api.testcalculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestCalculatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestCalculatorApplication.class, args);
	}
	
}