package br.com.calculator.api.testcalculator.entities;

import br.com.calculator.api.testcalculator.enums.PersonEatsEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class PersonEatsEntity {

    @Id
    private String id;

    private PersonEatsEnum person;
    private Double amount;
}