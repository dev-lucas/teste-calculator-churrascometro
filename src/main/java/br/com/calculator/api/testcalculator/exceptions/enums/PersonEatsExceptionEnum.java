package br.com.calculator.api.testcalculator.exceptions.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum PersonEatsExceptionEnum {

    PERSON_EATS_EXCEPTION_ENUM("CPE001", "Person Eats not found", 400);//Calculator Person Eats 001

    private String code;
    private String message;
    private Integer statusCode;
}