package br.com.calculator.api.testcalculator.entities;

import br.com.calculator.api.testcalculator.enums.QuantityEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class QuantityEatEntity {

  @Id
  private Long id;

  private QuantityEnum quantityEnum;
  private Double percentage;

}