package br.com.calculator.api.testcalculator.entities;

import br.com.calculator.api.testcalculator.enums.FoodEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class PercentageBarbecueEntity {

  @Id
  private Long id;

  private FoodEnum foodEnum;
  private Double percentage;

}