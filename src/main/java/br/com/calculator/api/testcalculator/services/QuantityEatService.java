package br.com.calculator.api.testcalculator.services;

import br.com.calculator.api.testcalculator.entities.QuantityEatEntity;

import java.util.List;

public interface QuantityEatService {

  List<QuantityEatEntity> findAll();
  QuantityEatEntity findById(Long id);

}