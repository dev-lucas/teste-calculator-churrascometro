package br.com.calculator.api.testcalculator.exceptions;

import br.com.calculator.api.testcalculator.exceptions.enums.PersonEatsExceptionEnum;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class PersonEatsException extends CalculatorApiException {
  private static final long serialVersionUID = -4589179341768493322L;
  private final PersonEatsExceptionEnum error;

  public PersonEatsException(PersonEatsExceptionEnum error) {
      super(error.getMessage());
      this.error = error;
  }
}